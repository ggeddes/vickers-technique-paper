import numpy as np
from spacepy.time import Ticktock
import spacepy.coordinates as coord
from fortranformat import FortranRecordReader
import traceback

def msis_read(file,version):
      '''
      MSIS array for Vickers, format:0- alt[msis5], 1-iri temp [~neutral?MSIS12],2- o[MSIS08],3- n2[MSIS09],4- o2[MSIS10],5- O+,6- S0
      if this starts failing, maybe the BeautifulSoup package will parse the HTML better.
      '''
      if version=='00':
          h,f=20,5
      elif version=='90':
          h,f=21,16
      else: raise Exception('please specify an MSIS version')
      MSIS=np.genfromtxt(file,skip_header=h,skip_footer=f)
      print('MSIS SHAPE: '+str(MSIS.shape))
      return MSIS

def chi_sq_stat(x,y,y_err):
    result=np.sum((y-x)**2/y_err**2)
    dof=np.size(y)
    #print(result)
    return  {'chi^2':result,'dof':dof}


def set_only_once(o,a,value):
    """Sets attribute named a (string) of object o (object reference) to value if and only if o.a does not already exist."""
    if not hasattr(o,a): setattr(o,a,value)
    
def calc_tangent_point_zenith_angles(satellite_height, altitudes):
    """Takes an array of altitudes and returns an array of tangent point zenith angles for the satellite height."""
    return zenithangle(satellite_height, altitudes)

def calc_tangent_alts_from_zenith_angles(satellite_height, zenith_angles):
    """Takes an array of zenith angles and returns an array of tangent point altitudes given the satellite height."""
    return tanheight(satellite_height, zenith_angles)


def zenithangle(h,tanptalt): # from Ewan's raids_functions.py
	#    print('warning,  function not carefully validated')
    #print("satellite height is:"+str(h))
    #takes the satellite height (h) and tangent point heights(s) of a line of sight
    # returns the zenith angle observed by the satellite in degrees.
    if np.size(h) != 1:
        return 'height error, currently function requires one height'
    RE=6371.0 #mean Earth radius
    '''WRONG approach:
    x=np.sqrt((h+RE)**2-RE**2)
    angleoglobe= np.arccos(RE/(RE+h))
    phi=np.arctan(tanptalt/x)
    ZA_radians = -phi+np.pi/2.0 + angleoglobe
    print(x, phi*60.0,angleoglobe*60.0)
    print((phi+np.pi/2.0)*180.0/np.pi)
    '''
    angleoglobe = np.arccos((RE+np.array(tanptalt))/(RE+h))
    ZA_radians=np.pi/2.0 + angleoglobe
    # print(ZA_radians,angleoglobe)
    return ZA_radians*180.0/np.pi

def tanheight(h,ZA): # from Ewan's raids_functions.py
    #print("satellite height is:"+str(h))
    #takes the satellite height (h) and zenith angle(s) of line of sight
    # returns the tangent height of line of sight from the earth.
    if np.size(h) != 1:
        return 'error'
    ZA_radians=ZA*np.pi/180.0
    RE=6371.0 #mean Earth radius
    '''  x=np.sqrt((h+RE)**2-RE**2)
    angleoglobe= np.arccos(RE/(RE+h))
    phi=np.pi/2.0 - ZA_radians + angleoglobe
    # print(x, phi*60,angleoglobe*60)
    '''
    htanpt=np.cos(ZA_radians-np.pi/2.0)*(RE+h)-RE
    return htanpt #x*np.tan(phi)


def localtime(UTC,LON): # from Ewan's raids_functions.py
    #modeled on localtime.pro, http://coco.atmos.washington.edu/ion_script/gamap2/date_time/localtime.pro
    LON=np.array(LON)
    UTC=np.array(UTC)
    ind=np.where(LON>180.0)[0]
    if len(ind)>0:
        LON[ind]=LON[ind]-360.0
    LocTime=np.mod(np.float(UTC)+np.float(LON)/15.0,24.0)
    
    ind=np.where(LocTime<0)[0]
    print(ind)
    if len(ind) >0:
        LocTime[ind]=LocTime[ind]+24
    return LocTime


def get_auric_profile(emission_fname,source_list,n_auric_rows=16,sum_lines=True): # from Ewan's RAIDS_auric_functions
    #units are not necc. flux, flux here is just typical unit of observable.
    recordtype=[]
    altitudes=[]
    zas=[]
    source_dict={}
    for source in source_list:
        fluxes=[]
        row=1e30

        f=open(emission_fname,'r')
        for i, line in enumerate(f):
            if 'Altitudes (km)' in line:
                if not len(altitudes)>0:
                    recordtype.append([line,i])
                    header_line=FortranRecordReader('(6F12.2)')
                    array=altitudes
                    row=0
                    continue
            if 'Zenith Angles (deg)' in line:
                recordtype.append([line,i])
                header_line=FortranRecordReader('(6F12.2)')
                array=zas
                row=0
                continue
            if source in line:
                recordtype.append([line,i])
                header_line=FortranRecordReader('(6E12.3)')
                array=fluxes
                row=0
                continue
            if row < n_auric_rows+1:
                try:
                    value=header_line.read(line)
                    #  print(value)
                    array.append(value) #python magic, the if statements made array the right array depending on the case, no pointers needed, every named variable is really a reference
                    row=row+1
                except Exception:
                    print('Failed to read line #'+str(i))
                    traceback.print_exc()
                    continue
        source_dict.update({source:np.nan_to_num(None2NaN(np.array(fluxes).flatten()))})
        f.close()
        #print(altitudes)
    altitudes=    np.nan_to_num(None2NaN(np.array(altitudes).flatten()))
    if not sum_lines:
        source_dict
        return {'Sources':source_dict,'altitudes':altitudes}
    
    if sum_lines:
        #print source_dict
        sum_array=np.zeros(len(source_dict.values()[0].flatten()))
        
        #print(sum_array)
        for s in source_dict:
            source_array=np.nan_to_num(None2NaN(np.array(np.array(source_dict[s])).flatten()))
            #print(source_array.shape,sum_array.shape)
            
            if (sum_array.shape==source_array.shape) and (np.sum(source_array) >0):
                sum_array=source_array+sum_array
            else:
                print('array size error.')
        return [sum_array, altitudes, zas]
        
        
        
        
        
        
def geo2mag(alt,lat,lon): # from Ewan's RAIDS_auric_functions
    #call with altitude in kilometers and lat/lon in degrees
    #example:
    #magcoord=geotomag(340,64.53,-50.125).data[0]
    #mlat=magcoord[1]
    #mlon=magcoord[2]
    Re=6371.0 #mean Earth radius in kilometers
    #setup the geographic coordinate object with altitude in earth radii 
    cvals = coord.Coords([np.float((alt/Re+Re))/Re,np.float(lat),np.float(lon)], 'GEO', 'sph',['Re','deg','deg'])
    #set time coordinates are correct:
    cvals.ticks=Ticktock(['2012-01-01T12:00:00'], 'ISO')
    #return the magnetic coords in the same units as the geographic:
    return cvals.convert('MAG','sph')
    
    
def mod_auric_params(directory, paramdict): # modified from Ewan's RAIDS_auric_functions
    new_file=open(directory+'/newparam.inp','w')

    with open(directory+'/param.inp') as f:
        for line in f.readlines():
        #prototype of replacing a value with a new number (13)
        #replace the line preserving position and white space.
            if proc_line(line,paramdict,new_file):
                continue
            else:
            #if never broke out of loop, write the original to disk
                new_file.write(line)
                #  print('didnt break free')
        
    new_file.close()
    return None
    
def proc_line(line,paramdict,new_file):
    for parameter in paramdict:
        #print(parameter,line)
        if parameter in line:
            # print(parameter,line)
            updateline(line,new_file,str(np.round(paramdict[parameter],2)))
            #print('updated')
            return True
        else:
            continue
        #if not a line of interest:
        return False
        
def updateline(line,new_file,newvalue):
    newline= line.replace(str(float(line[15:24])).rjust(6),newvalue.rjust(6))
    # print(line,newline)
    new_file.write(newline)

def None2NaN(x):
    """Converts None objects to nan, for use in
       NumPy arrays. Returns an array.
       http://scienceoss.com/convert-none-to-nan-for-use-in-numpy-arrays/"""
    newlist = []
    for i in x:
        if i is not None:
            newlist.append(i)
        else:
            newlist.append(np.nan)
    return np.array(newlist)

class isr:
    def __init__(self, name, lat, lon,latCGM,lonCGM,ionosonde_URSI):
        #self.T_sys = T_sys #kelvin
        self.name = name #station name
        self.lat = lat #deg
        self.lon = lon #deg
        self.latCGM = latCGM #sec
        self.lonCGM = lonCGM #sec
                             #self.x,self.y = map(self.lon,self.lat) #for mapping to basemap.
        self.ionosonde_URSI = ionosonde_URSI
        #   = map(self.lat,self.lon)[1]
    def __repr__(self):
        s='Ionosonde at "{}".\n'.format(self.name)
        s+='URSI: {}\n'.format(self.ionosonde_URSI)
        s+='Latitude: {}\n'.format(self.lat)
        s+='Longitude: {}\n'.format(self.lon)
        return s
         
                             #http://www.iacg.org/iacg/ground_stations/ics.html:

#Not an exhaustive list, LOOKING FOR DIGISONDES => DPS-4 AT MID-LOW LATITUDES, n
def radars():
    radar_dict={'JI91J': isr('Jicamarca',-11.9,284.0,3.3,355.6,'JI91J'),
            'Arecibo': isr('Arecibo',18.3,293.3,29.0,9.1,False),
            'MHJ45': isr('Millstone Hill',42.6,288.5,53.2,6.1,'MHJ45'),
            'TR169':isr('EISCAT -Tromso',69.6,19.2,66.5,103.9,'TR169'),
            'Kiruna':isr('EISCAT-Kiruna',67.9,20.4,	64.7,103.4,False),
            'Sodankyla': isr('EISCAT-Sodankyla',67.4,26.6,63.8,108.0,False),
            'SMJ67' : isr('Sondrestrom',67.0,309.0,73.5,41.2,'SMJ67'),
            'SAA0K': isr('Sao Luis, Maranhao',-2.528333,315.696,0.95,29.63,'SAA0K'),
            'KJ609':isr('KWAJALEIN',9.0,167.2,0,0,'KJ609'),
            'PA836':    isr('Point Arguello-Vandenberg',34.8,239.5,0,0,'PA836'),
            "JR005":    isr('Juliusruh, Germany',54.6,13.4,0,0,'JR005'),
            'CGK21':       isr('Campo Grande, Brazil',-20.5,305,0,0,'CGK21'),
            'GA762':      isr('Gakona, AK',62.38,215,0,0,'GA762'),
            'EB040':       isr('Roquetes, Spain',40.8,0.5,0,0,'EB040'),
            #isr('Kirtland AFB, NM',35.1,235.4,0,0,'KR835'),
            'DB049':     isr('Dourbes, Belgium',50.1,4.6,0,0,'DB049'),
            'IC437':     isr('I-CHEON, South Korea',37.14,127.54,0,0,'IC437'),
            'PQ052':        isr('Prohuhonice, Czech Republic',50.0,14.6,0,0,'PQ052'),
            'HA419':       isr('HAINAN, CHINA',19.4,109,0,0,'HA419'),
            'BP440':        isr('BEJING',40.3,116.2,0,0,'BP440'),
            'JJ433':     isr('Jeju Island, Korea',33.43,126.3,0,0,'JJ433'),
            'IL008':        isr('Ilorin, Nigera',8.5,4.5,0,0,'IL008'),
            'FZA0M':      isr('Fortaleza, Brazil',-3.9,321.6,0,0,'FZA0M'),
            'AT138':       isr('Athens',38.0,23.5,0,0,'AT138'),
            'LV12P':        isr('Louisvale, South Africa',-28.5,21.2,0,0,'LV12P'),
            'GR13L':        isr('Grahamstown, South Africa',-33.3,26.5,0,0,'GR13L'),
            'HE13N':      isr('Hermanus, South Africa',-34.42,19.22,0,0,'HE13N'),
            'PSJ5J':      isr('Port Stanley, Falkland Islands',-51.6,302.1,0,0,'PSJ5J'),
        }
    return radar_dict
