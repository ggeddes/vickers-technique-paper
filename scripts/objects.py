import numpy as np
import subprocess, threading, os, re
from datetime import datetime, timedelta
from scipy.stats import linregress
from scipy.interpolate import interp1d, griddata
from static_functions import localtime, mod_auric_params, geo2mag, get_auric_profile, set_only_once
from static_functions import calc_tangent_alts_from_zenith_angles, calc_tangent_point_zenith_angles
from static_functions import radars, msis_read
from glob import glob

class Observable( object ):
    """Base class for a quantity with units."""
    unitdict = {"":1}
    def __init__(self, units):
        self.units = units
    
    @classmethod
    def ulookup(cls, unit):
        """Looks up the factor associated with 'unit' in the class dictionary"""
        return cls.unitdict[unit]

    @property
    def ufactor(self):
        """Returns the numerical factor needed to make units consistent"""
        return self.ulookup(self.orig_units)/self.ulookup(self._units)
    
    @property
    def units(self): return self._units
    
    @units.setter
    def units(self, u):
        self._units = u
        set_only_once(self,"_orig_units", ''.join(u)) #join forces a new string to be made.

    @property
    def orig_units(self): return self._orig_units

class Location( object ):
    NS = {0:'N',1:'N',-1:'S'}
    EW = {0:'E',1:'E',-1:'W'}
    def __init__(self, glat=None, glon=None, mlat=None, mlon=None ):
        if glat: self.glat = glat # geolatitude
        if glon: self.glon = glat # geolongitude
        if mlat: self.mlat = mlat # geomagnetic latitude
        if mlon: self.mlon = mlon # geomagnetic longitude
        
    @property
    def geostring(self):
        """Returns geographic coordinates as a formatted string."""
        return "{lat:02.0f}{NS}{lon:03.0f}{EW}".format(lat=abs(self.glat),NS=self.NS[np.sign(self.glat)],
                                                    lon=abs(self.glon),EW=self.EW[np.sign(self.glon)])
        
    @property
    def glon(self): return self._glon%360 # must be positive to play nicely with modelweb

    @glon.setter
    def glon(self, new): self._glon = new
    
    @glon.deleter
    def glon(self): del self._glon
    
    @property # geomagnetic latitude
    def mlat(self): 
        if hasattr(self,"_mlat"):
            return self._mlat
        else: 
            return geo2mag(self.height,self.glat,self.glon).data[0][1]
            
    @mlat.setter
    def mlat(self, new): self._mlat = new
    
    @mlat.deleter
    def mlat(self): del self._mlat
    
    @property # geomagnetic longitude 
    def mlon(self):
        if hasattr(self,"_mlon"):
            return self._mlon
        else:
            return geo2mag(self.height,self.glat,self.glon).data[0][2]
            
    @mlon.setter
    def mlon(self, new): self._mlon = new
    
    @mlon.deleter
    def mlon(self): del self._mlon

class Profile( Observable ):
    """A Profile is some function of altitude. To save memory, pass an array for alts, and do not slice it, i.e. don't do: p=Profile(alts[:],vals[:])"""
    unitdict = {"":1}
    def __init__(self, altitudes, values, error=None, units="", source_file=""):
        self.alts   = altitudes
        self.values = values[:]
        self.error  = error
        self.units  = units
        self.source = source_file
    
    def __str__(self):
        return "{} profile from {}".format(self.__class__.__name__,self.source)
 
    def easy_interp( self,new_alts, method='linear', **kw ):
        """Interpolates the values to a new set of altitudes using the original data set. This way, you do not need to worry about interpolating multiple times."""
        if len(self.alts)==len(self.orig_alts):
            if (self.alts==self.orig_alts).all():
                self.interp = interp1d(self.alts,self._values,kind=method,fill_value=np.max(self.orig_values)*1e-10,bounds_error=False, **kw)
        else:
            self.interp = interp1d(self.orig_alts,self.orig_values,kind=method,fill_value=0,bounds_error=False)
        self.values = self.interp( new_alts )
        self.alts   = new_alts
    
    def convert(self, new_unit): self.units = new_unit

    def restore(self):
        self.values = self.orig_values
        self.alts   = self.orig_alts    
        
    def extend_top(self):
        """Extends the topside of the profile using a constant scale height estimated using the last 3 nonzero values"""
        p=3
        n=np.max(np.where(self._values>0))+1
        slope, intercept = linregress(self._alts[n-p:n],np.log(self._values[n-p:n]))[0:2]
        self._values[n:] = np.exp(intercept + slope*self._alts[n:]) #set _values directly here because of the slice. 
    
    def exp_floor(self):
        """Add a very small, decaying curve to the profile, because this seems to be one way to appease gsrp."""
        self.values = self._values + np.max(self._values)*1e-10*np.exp(-self._alts/np.max(self.alts))
                   
    @classmethod
    def fromtxt(cls, filename, usecols=(0,1), units="", **kwargs):
        """Constructor for text file input."""
        assert units in cls.unitdict.keys(), "'{}' is not a unit of {}.".format(units,cls.__name__)
        data = np.nan_to_num(np.genfromtxt(filename,usecols=usecols, **kwargs))
        return cls(data[:,0], data[:,1], units=units, source_file=filename)
    
    @property
    def values(self): return self._values*self.ufactor
    
    @values.setter
    def values(self, new):
        self._values = new
        set_only_once(self, "_orig_values",np.copy(new).squeeze())
        
    @values.deleter
    def values(self): del self._values

    @property
    def alts(self): return self._alts
    
    @alts.setter
    def alts(self, new):
        self._alts = new        
        set_only_once(self, "_orig_alts",np.copy(new).squeeze())

    @alts.deleter
    def alts(self): del self._alts    
            
    @property
    def error(self): return self._error*self.ufactor
    
    @error.setter
    def error(self, new): self._error = new
    
    @error.deleter
    def error(self): del self._error
    
    @property
    def orig_alts(self): return np.copy(self._orig_alts)
        
    @property
    def orig_values(self): return np.copy(self._orig_values)
        
class AngularProfile( Profile ):
    """For profiles that you may want to view as functions of zenith angle.
    
    Confusingly, AngularProfile puts zenith angle and altitude on equal footing, i.e. you don't need to specify angles."""
    def __init__(self, altitudes, values, error=None, units="", angles=False, satellite=300, source_file=""):
        """Pass angles=True if the 'altitudes' are actually zenith angles."""
        self.angles=angles
        self.height=satellite
        if angles == True:
            self.za = altitudes
            alts = calc_tangent_alts_from_zenith_angles(self.height, self.za)
            Profile.__init__(self, alts, values, error, units, source_file)
        else:
            Profile.__init__(self, altitudes, values, error, units, source_file)
            self.za=self.za # weird looking, but this will call the getter to derive it and then the setter store it.
            
    def easy_interp(self, *args, **kw ):
        if self.angles: self.interp_angles( *args, **kw)
        else: Profile.easy_interp(self, *args, **kw )
    
    def interp_angles(self, new_angles, method='linear'):
        assert self.angles==True, "Interpolate using the altitudes instead."
        self.interp = interp1d(self._orig_za,self._orig_values,kind=method,fill_value=np.max(self.orig_values)*1e-10,bounds_error=False)
        self.values = self.interp( new_angles )
        self.za = new_angles
    
    @property
    def za(self):
        if self.angles: return self._za
        else: return calc_tangent_point_zenith_angles(self.height, self.alts)
    
    @za.setter
    def za(self, za): 
        self._za=za
        set_only_once(self, "_orig_za",np.copy(za).squeeze())
  
    @za.deleter
    def za(self): del self._za
    
    @Profile.alts.getter # http://stackoverflow.com/questions/7019643/overriding-properties-in-python 
    def alts(self): 
        if self.angles: return calc_tangent_alts_from_zenith_angles(self.height, self.za)
        else: return self._alts

class Density( Profile ):
    """Profile subclass for densities."""
    unitdict = {"":1,"cm-3":1,"m-3":1e-6}
    
    @classmethod
    def fromobs(cls, obs, folder, units='m-3'):
        """Searches *folder* for the best ionosonde measurement to go with *obs*."""
        fname = obs.find_closest_ionosonde( folder )
        if fname: 
            return cls.fromtxt( fname, units=units )
        else: 
            return None
    
    @classmethod
    def fromfunction(cls, altitudes, f,units=""):
        """creates a density profile from a set of altitudes and a parameterization."""
        return cls(altitudes,np.vectorize(f)(altitudes),units=units)
    
    @classmethod
    def fromchapman(cls,altitudes,N,h,H,*p):
        """Returns a density from a set of chapman parameters.
        For now, pass N, then h, then H."""
        if len(p) == 0:
            chap = Chapman(N=N,h=h,H=H)
            return cls.fromfunction(altitudes, chap, units="")
        elif len(p) == 2:
            print "can't do 5 just yet"

class Temperature( Profile ):
    """Profile subclass for neutral temperatures."""
    unitdict = {"":1,"K":1}    
    
class Intensity( AngularProfile ):
    """Profile subclass for intensities."""
    unitdict = {"":1,"rayleighs":1,"photons":1e-6*(4.0*np.pi)} #"photons" = photons/sec/cm^2/steradian
    @classmethod
    def frompickle(cls, loaded_pickle, satellite, units="photons", source_file="" ):
        """Constructor for freshly unpickled profiles."""
        return cls(np.array(loaded_pickle['altitudes']), np.array(loaded_pickle['euvprofile']), np.array(loaded_pickle['euvprofile_err']),
                    units=units, angles=False, satellite=satellite, source_file=loaded_pickle['filename'])
        
    
class Emission( AngularProfile ):
    """"Profile subclass for volume emission rates"""
    unitdict = {"":1,"cm-3s-1":1}
    
    def griddata(self, new_alts, **kw):
        fill = np.max(self.values)*1e-10
        y = griddata(self.alts,self.values,new_alts, fill_value=fill, **kw)
        self.alts = new_alts
        self.values = y
    
    @classmethod
    def fromauric(cls, auric, obs, sw,
                    features=['O+e 832 A (initial)','O+e 833 A (initial)','O+e 834 A (initial)',
                              'O+hv 832 A (initial)','O+hv 833 A (initial)','O+hv 834 A (initial)'],
                    scale=1):
        """Constructor for auric source"""
        assert isinstance(auric, AURICManager), "Expected AURICManager, got '{}'".format(auric)
        auric_profile, auric_alts = auric.run(obs, sw, features=features)
        return cls(auric_alts, auric_profile*scale, units="cm-3s-1",source_file="AURIC")    
 
 
class ObservationParameters( Location ): # inherits mlat/mlon from Location.
    """Location information corresponding to an observation."""
    radar_dict = radars()
    def __init__(self, geolat, geolon, obsdate, satellite=300, tanptsza=0, radar=''):
        self.glat = geolat       # geolatitude
        self.glon = geolon       # geolongitude
        self.date = obsdate      # observation date [and time]
        self.height = satellite  # satellite height
        self.tanptsza = tanptsza # tangent point solar zenith angle
        self.radar = radar # radar, if applicable
        
    def __str__(self):
        return "Observation at {} N, {} E on {}".format(np.round(self.glat,2), np.round(self.glon,2), self.date)    
                        
    def find_closest_ionosonde( self, folder ):
        # glob files from the same location:
        files = glob(folder+"/*"+self.geostring+"*SAO.txt")
        if self.radar:
            files.extend(glob(folder+"/*"+self.radar+"*SAO.txt"))
        closest = datetime(1900, 1, 1)
        ind = None
        # grab the date and time from the filenames:
        for i, name in enumerate(files):
            m = re.search('[0-9]{14,14}',name) # grab the first 14 digit number.
            s = m.group(0)
            dt = datetime(int(s[0:4]),int(s[4:6]),int(s[6:8]),int(s[8:10]),int(s[10:12]),int(s[12:14]))
            # find the closest date:
            if abs(self.date - dt) < abs(self.date - closest):
                closest = dt
                ind = i
        try:
            print "closest ionosonde measurement: {}, time difference of {}".format(files[ind],self.date-closest) 
            return os.path.abspath(files[ind])
        except TypeError:
            print "no ionosonde found for {}".format(self)
            return ""

    @classmethod
    def frompickle(cls, loaded_pickle, satellite): #Not really form a pickle, it's from a dictionary. These methods could be cleaner.
        """Constructor for fetching parameters from a .pkl file loaded with cPickle."""
        n=loaded_pickle['filename']
        dt = datetime( int(n[6:10]),int(n[10:12]),int(n[12:14]),int(n[14:16]),int(n[16:18]) )
        try: # quick fix
            return cls.fromARBIT(loaded_pickle, dt, satellite) 
        except KeyError:
            return cls.fromISR(loaded_pickle, dt, satellite)
    
    @classmethod
    def fromARBIT(cls, loaded_pickle, obsdate, satellite ):
        """Constructor for ARBIT*** pickles"""
        return cls(loaded_pickle['geo']['mean lat'], loaded_pickle['geo']['mean lon'],
                    obsdate=obsdate, satellite=satellite,tanptsza=loaded_pickle['geo']['meansza'], radar = "ARBIT")   

    @classmethod
    def fromISR(cls, loaded_pickle, obsdate,  satellite):
        """Constructor for pickles with an ISR name"""
        n = loaded_pickle['filename']
        radar = cls.radar_dict[n[0:5]]
        import ephem        
        obs=ephem.Observer()
        obs.lat=radar.lat*np.pi/180
        obs.lon=radar.lon*np.pi/180
        obs.elevation=satellite*1000
        obs.date=obsdate
        sun=ephem.Sun()
        sun.compute(obs)
        tanpt_sza =  (np.pi/2-sun.alt)*180/np.pi
        return cls(radar.lat, radar.lon, obsdate, satellite, tanptsza=tanpt_sza, radar=radar.ionosonde_URSI)

    @property # solar local time
    def slt(self): return localtime(self.glon,self.date.hour + self.date.minute/60.0)
                        
class SpaceWeather( object ):
    """A collection of space weather parameters."""
    _celestrak_list =['BSRN', 'ND', 'Kp03', 'Kp36', 'Kp69', 'Kp912', 'Kp1215', 'Kp1518', 'Kp1821', 'Kp210', 'Sum_Kp', 
                'Ap03',  'Ap36',  'Ap69',  'Ap912',  'Ap1215',  'Ap1518',  'Ap1821',  'Ap210',  'Avg_Ap' ,
                'Cp' ,'C9' ,'ISN', 'F107Adj', 'Q', 'F107Ctr81Adj', 'F107Lst81Adj', 'F107Obs', 'F107Ctr81Obs', 'F107Lst81Obs']
    
    def __init__(self, data, source_file=""):
        self.data = data 
        self.source = source_file
        self.param_list=[]
        
    def __call__( self, date, param ):
        """Returns the value of param on day/month/year."""
        assert isinstance(date, datetime), "date must be a datetime object"
        assert param in self._celestrak_list, "Parameter name '{}' not recognized".format(param)
        self.param_list = self._celestrak_list
        return self.data[np.where((self.data['yyyy']==date.year) & (self.data['mm']==date.month) & (self.data['dd']==date.day))][param][0]       

    @classmethod
    def fromtxt(cls, filename, **kw):
        """Constructor for loading data from text files from http://celestrak.com"""
        data = np.genfromtxt(filename,
                        names=(
                        'yyyy', 'mm', 'dd', 'BSRN', 'ND', 
                        'Kp0-3', 'Kp3-6', 'Kp6-9', 'Kp9-12', 'Kp12-15', 'Kp15-18', 'Kp18-21', 'Kp21-0', 'Sum Kp', 
                        'Ap0-3',  'Ap3-6',  'Ap6-9',  'Ap9-12',  'Ap12-15',  'Ap15-18',  'Ap18-21',  'Ap21-0',  'Avg Ap' ,
                        'Cp' ,'C9' ,'ISN', 'F10.7Adj', 'Q', 'F10.7Ctr81Adj', 'F10.7Lst81Adj', 'F10.7Obs', 'F10.7Ctr81Obs', 'F10.7Lst81Obs'), **kw)
        return cls( data )

 
class MSIS( object ):
    """ This object holds the MSIS model run profiles"""
    def __init__(self, altitudes, T, O, N2, O2, version='00'):
        self.alts = altitudes
        self.T  = Temperature(self.alts, T)
        self.O  = Density(self.alts, O)
        self.N2 = Density(self.alts, N2)
        self.O2 = Density(self.alts, O2)
        self.version=version
    
    @staticmethod
    def generate_post( obs, start, stop, step ):
        post='model=msis' #form the post request...
        post+='&year={}&month={}&day={}'.format(obs.date.year,obs.date.month,obs.date.day)
        post+='&time_flag=0' #0 for universal time, 1 for local time
        post+='&hour={}'.format(obs.date.hour)
        post+='&geo_flag=0' #0 for geographic, 1 for geomagnetic
        post+='&latitude={}&longitude={}'.format(obs.glat,obs.glon)
        post+='&height=100.'
        post+='&profile=1' #specifies that we want a height profile
        post+='&start={}&stop={}&step={}'.format(float(start),float(stop),float(step)) #altitude bins (must be floats, not integers)
        post+='&format=0'#0 list, 1 plot, 2 ASCII, 3 XML
        post+='&vars=05' #choose height as the independent variable
        post+='&vars=12&vars=08&vars=09&vars=10' #calculate neutral temp and O, N2, O2 number densities, respectively
        return post
        
    @staticmethod
    def curl( obs,  version='00' ):
        """Checks if an msis run is already available and requests it from the web if not. Returns a return code and the filename of the msis run."""
        #use a local MSIS file if it exists (if you change MSIS parameters, need to delete the files so new parameters are downloaded.)
        msis_fname='msise{}_curl'.format(version)+'_'+obs.geostring+'_'+str(obs.date.date()).replace('-','_')+'.txt'
        msis_full=msis_fname[:-4]+'_full.txt'
        if os.path.exists(msis_full):
            return 0, msis_full
        if version=='00':
            msis_url='http://ccmc.gsfc.nasa.gov/cgi-bin/modelweb/models/nrlmsise00.cgi' #msis 2000 address
        elif version=='90':
            msis_url='http://omniweb.gsfc.nasa.gov/cgi/vitmo/vitmo_model.cgi' #msis 1990 address
        else: raise Exception("Unrecognized MSIS version, '{}'".format(version))
        posts=[]
        posts.append(MSIS.generate_post(obs,100,500,10))
        posts.append(MSIS.generate_post(obs,510,1000,10))
        try:
            p = 0
            for i, post in enumerate(posts):
                assert re.search("[^|; ]*[^(&&)]*",post).group(0) == post, "Is this a valid post request?\n{}".format(post)
                assert re.search("[^|;& ]*",msis_fname).group(0) == msis_fname, "Something fishy going on with {}?".format(msis_fname) # Bad things are possible with shell=True.
                assert re.search("[^|;& ]*",msis_url).group(0) == msis_url, "Is this a real url?\n{}".format(msis_url)
                p+=subprocess.call('curl -v -d "{}" {} > {}'.format(post,msis_url,str(i)+msis_fname), shell=True)
        except subprocess.CalledProcessError as e:
            p = e.returncode
            print e
            raise e
        M = np.empty((0,5))
        for i in (0,1):
            M = np.append(M, msis_read(str(i)+msis_fname,version))
        M= M.reshape((M.size/5,5))
        np.savetxt(msis_full, M)
        return p, msis_full
        
    @classmethod
    def fromtxt( cls, filename, **kwargs ):
        """Constructor for reading msis from a text file"""
        m=np.genfromtxt(filename, **kwargs)#msis_read(filename, version)
        return cls(m[:,0], m[:,1], m[:,2], m[:,3], m[:,4], filename[5:7])
        
    @classmethod
    def fromobs( cls, obs, data_folder, version='00' ):
        """Constructor for locating an appropriate msis file to go with an observation."""
        with visit(data_folder):
            returncode, filename = cls.curl( obs, version )
            return cls.fromtxt( filename )
    
    @classmethod
    def fromweb( cls, obs, version ):
        """Not implemented"""
        pass        
                  
  
class AURICManager( object ):
    """This class contains methods for running and reading AURIC model runs."""
    auricroot = str(os.getenv("AURIC_ROOT"))+"/"
    def __init__(self, directory=auricroot):
        assert os.path.isdir(directory), "Invalid AURIC path, '{}'".format(directory)
        assert os.path.exists(directory + "/onerun.sh"), "Looks like AURIC needs to be set up."
        self.path = os.path.abspath(directory)
        self.batch_command = Command("bash " + self.path + "/onerun.sh")
        # add some logic to see if auric needs to be setup
     
    def run(self, obs, sw, features=[]): #this prints a traceback, but seems to run fine?
        """Runs AURIC model and returns a volume emission rate profile. """
        self.update_params(obs, sw)
        self.update_view(obs.height, np.linspace(95,115,num=100))
        with visit(self.path):
            self.write_radtrans_options({})#NOT A GOOD FIX FOR THIS. MAKE THIS MORE VERSATILE LATER
            self.batch_command.run(timeout=10)
        return self.read(features)
        
    def read(self, features):
        auric_profile_pe,auric_alts_pe,auriz_zas_pe = get_auric_profile(self.path+'/dayglo.ver',features,sum_lines=True)
        return auric_profile_pe, auric_alts_pe        

    def update_params(self, obs, sw ):
        data = self.read_auric_params("param.inp")
        self.modify_auric_params(data,
            {'GLAT':obs.glat,'GLON':obs.glon,'GMLAT':obs.mlat,
                'GMLON':obs.mlon,'SZA':obs.tanptsza,'SLT':obs.slt,
                'F10DAY':sw(obs.date, 'F107Adj'),
                'F10PRE':sw(obs.date+timedelta(-1), 'F107Adj'),
                'F10AVE':sw(obs.date,'F107Lst81Adj'),
                'AP(1)':sw(obs.date, 'Avg_Ap'), 
                'AP(2)':-1, # current 3-hour Ap
                'AP(3)':-1, # 3-hour Ap for 3 hours ago
                'AP(4)':-1, # 3-hour Ap for 6 hours ago
                'AP(5)':-1, # 3-hour Ap for 9 hours ago
                'AP(6)':-1, # average of 8 3-hour ap indices from 12 to 33 hours prior to current time
                'AP(7)':-1})# average of 8 3-hour ap indices from 36 to 57 hours prior to current time
                # the AP -1 values mean AURIC will use the daily average AP, user manual, page 23
        self.write_auric_params("param.inp",data)

    def read_auric_params(self, filename):
        """Reads AURIC's param.inp file and returns a list containing the unformatted data one each line."""
        with open(self.path+"/"+filename,'r') as f:
            lines=f.readlines()        
        parsed_lines=[]
        for i, line in enumerate(lines):
            if re.search(":$",line): 
                parsed_lines.append(line) 
                continue
            x=re.search('.*(?==)',line)                         # Everything before =
            y=re.search('(?<==)*[0-9 -]*?\.*[0-9 ]*(?=:)',line) # Number between = and :
            z=re.search('(?<=:).*',line)                        # Anything after :
            parsed_lines.append([x.group(0).strip(), float(y.group(0).strip()),z.group(0).strip()])
        return parsed_lines
    
    def modify_auric_params(self, parsed_lines, paramdict):
        """Takes a list of parsed lines from read_auric_params and makes the changes specified in paramdict."""
        for line in parsed_lines: # Replace values to be modified
            if line[0] in paramdict.keys():
                line[1] = paramdict[line[0]]
    
    def write_auric_params(self, filename, parsed_lines):
        """Writes a set of lines parsed by read_auric_params to *filename*."""
        intkeys = ['NALT', 'YYDDD'] 
        lines=[None]*32
        for i, line in enumerate(parsed_lines):
            if isinstance(parsed_lines[i], str):
                lines[i]=parsed_lines[i]
            elif parsed_lines[i][0] in intkeys:
                lines[i] = "{: >12s} = {: >10.0f} : {:<s}\n".format(*parsed_lines[i])
            else:
                lines[i] = "{: >12s} = {: >10.2f} : {:<s}\n".format(*parsed_lines[i])
        with open(self.path+"/"+filename,'w') as f:
            for line in lines:
                f.write(line)
                    
    def update_view(self, new_height, za=None ):
        """Modify view.inp"""
        with visit(self.path):
            if za is None:
                za = self.read_view()[1]
            self.write_view(new_height, za)
    
    def read_view(self, filename="view.inp"):
        """Read view.inp and return observing height in km and a list of zenith angles"""
        with open(filename,'r') as f:
            lines=f.readlines()
        h = float(lines[0].split()[0].strip())
        za=[float(line.strip()) for line in lines[1:]]
        return h, za

    def write_view(self, h,za,filename="view.inp"):
        """Write a new view.inp file."""
        with open(filename,'w') as f:
            f.write("{: 11.4f}   observer altitude (km)\n".format(h))
            for i in za:
                f.write("{: 12.5f}\n".format(i))

    def read_radtrans_options(self,filename='radtrans.opt'):
        """Takes the name of a radtrans option file and returns a dictionary of the options."""
        with open(filename,'r') as f:
            lines = f.readlines()
        options={}
        for line in lines:
            m = re.search("([0-9]{3,4}) *\= (ON|OFF)",line) 
            if m:
                options[m.group(1)]=m.group(2)
        return options

    def write_radtrans_options( self,options, filename='radtrans.opt' ):
        """Takes a dictionary of options and a filename and writes a radtrans options file for use with AURIC."""
        keylist=['832', '833', '834', '1304', '1356', '1040', '1026', '989', '1048', '1066', '1135', '1199']
        for key in keylist:
            if key not in options.keys():
                options[key] = "OFF" #set any missing keys to OFF
        with open(filename,'w') as f:
            f.write("Options for code RADTRANS:\n")
            f.write("-------------------------------------------------------\n")
            for key in keylist:
                s=" "*9+"{:4s} = {}\n".format(key,options[key])
                f.write(s)
            f.write("-------------------------------------------------------\n")

     
class VickersManager( object ):
    """Manager for running the vickers model from a remote directory."""
    LOOKDIRS  = 180
    ALTLEVELS = 90
    SCALT     = 340
    
    def __init__(self, directory, package):
        assert os.path.isdir(directory), "Invalid RT path, '{}'".format(directory)
        self.path = directory
        self._RT_func = package.runRT_func
    
    def runRT(self, obs, msis, iono, source2, source3, source4, safe_method="extend_top"):
        """Takes in necessary inputs and returns a forward model of scattering. 
        
        The values of iono and source will be interpolated to the msis altitudes.
        
        This will fail if msis length is not ALTLEVELS+1."""
        assert len(msis.alts)==self.ALTLEVELS+1
        
        self.make_safe(iono, safe_method)
        iono.easy_interp(msis.alts)
        iono.convert("cm-3")
        
        for source in [ source2, source3, source4 ]:
            self.make_safe(source, safe_method)
            source.convert("cm-3s-1")
        s2 = griddata(source2.alts,source2.values+1e-10,msis.alts,fill_value=1e-10)
        s3 = griddata(source3.alts,source3.values+1e-10,msis.alts,fill_value=1e-10)        
        s4 = griddata(source4.alts,source4.values+1e-10,msis.alts,fill_value=1e-10)
        
        vickers_input      = np.zeros((self.ALTLEVELS+1,9), dtype='double')
        vickers_input[:,0] = msis.alts[:]
        vickers_input[:,1] = msis.T.values[:]
        vickers_input[:,2] = msis.O.values[:]
        vickers_input[:,3] = msis.N2.values[:]
        vickers_input[:,4] = msis.O2.values[:]
        vickers_input[:,5] = iono.values[:]
        vickers_input[:,6] = s2[:]
        vickers_input[:,7] = s3[:]
        vickers_input[:,8] = s4[:]

        forward = np.empty(self.LOOKDIRS,dtype='double')
        
        with visit(self.path):
            self._RT_func(vickers_input,forward)
        
        return Intensity(np.arange(self.LOOKDIRS,step=180/self.LOOKDIRS),forward,units="photons",angles=True,satellite=obs,source_file="RT")    

    def make_safe(self, iono, method):
        getattr(iono, method).__call__()
                      
    @property
    def path(self): return self._path
        
    @path.setter
    def path(self, directory): self._path = os.path.abspath(directory)
    
    @path.deleter
    def path(self): del self._path


class Chapman( object ):
    """Callable chapman function for the given parameters.
    Example usage: 
        f=Chapman(N,h,H)
        plot(altitudes, f(altitudes))"""
    def __init__(self, **p):
        for param in p.keys():
            setattr(self, param, p[param])
        
    def __call__(self,alts):
        return np.vectorize(lambda z: (self.N)*np.exp((1-(z-self.h)/self.H-np.exp(-(z-self.h)/self.H))/2))(alts)
        
    @classmethod
    def generator(cls,Nlist,hlist,Hlist):
        """Returns a generator expression for the set of chapman profiles with parameters in Nlist, hlist, Hlist."""
        return ( cls(N=N,h=h,H=H) for N in Nlist for h in hlist for H in Hlist ) 

class Command(object):
      #http://stackoverflow.com/questions/1191374/subprocess-with-timeout?lq=1
      def __init__(self, cmd):
            self.cmd  = cmd
            self.process = None
            
      def run(self, timeout):
            def target():
                  print 'Thread started'
                  print(self.cmd)
                  #   self.process = subprocess.Popen(self.cmd,stderr=subprocess.STDOUT, stdout=subprocess.PIPE)#, shell=True)
                  self.process = subprocess.check_call(self.cmd.split())#, shell=True)
                  #self.process.communicate()
                  print 'Thread finished'
                  
            thread = threading.Thread(target=target)
            thread.start()
            
            thread.join(timeout)
            if thread.is_alive():
                  print 'Terminating process'
                  self.process.terminate()
                  thread.join()
                  print self.process.returncode        

class gsrpManager( VickersManager ):    
    def runRT(self, obs, msis, iono, source2, source3=None, source4=None, safe_method="extend_top"):
        """Takes in necessary inputs and returns a forward model of scattering. 
        
        The values of iono and source will be interpolated to the msis altitudes.
        
        This will fail if msis length is not ALTLEVELS+1."""
        assert len(msis.alts)==self.ALTLEVELS+1
        
        self.make_safe(iono, safe_method)
        iono.easy_interp(msis.alts)
        iono.convert("cm-3")
        

        if source3 and source4:
            source = source2.values+source3.values+source4.values
        else:
            source = source2.values
        Source = Emission(source2.alts, source)
        self.make_safe(Source, safe_method)
        Source.convert("cm-3s-1")
        s0 = griddata(Source.alts,Source.values+1e-10,msis.alts,fill_value=1e-10)
        
        vickers_input      = np.zeros((self.ALTLEVELS+1,7), dtype='double')
        vickers_input[:,0] = msis.alts[:]
        vickers_input[:,1] = msis.T.values[:]
        vickers_input[:,2] = msis.O.values[:]
        vickers_input[:,3] = msis.N2.values[:]
        vickers_input[:,4] = msis.O2.values[:]
        vickers_input[:,5] = iono.values[:]
        vickers_input[:,6] = s0[:]

        forward = np.empty(self.LOOKDIRS,dtype='double')
        
        with visit(self.path):
            self._RT_func(vickers_input,forward)
        
        return Intensity(np.arange(self.LOOKDIRS,step=180/self.LOOKDIRS),forward,units="photons",angles=True,satellite=obs,source_file="RT")    

class visit( object ):
    """Allows you to go to a directory, execute some code, and then return to where you were.
    
    Usage: 
        with visit('../path/to/visit/') as d:
            something_that_needs_to_be_executed_in_d"""
    def __init__(self, directory):
        assert os.path.isdir(directory), "Invalid directory, '{}'".format(directory)
        self.cwd=os.getcwd()
        self.nwd=os.path.abspath(directory)
        
    def __enter__(self):
        os.chdir(self.nwd)
        return self.nwd
        
    def __exit__(self, type, value, traceback):
        os.chdir(self.cwd)
