;; emacs macros because AGU doesn't want tex macros to be used

(defun o+ ()
  (interactive)
  (insert "\\(\\mathrm{O}^+\\) ")
  )

(defun oii834 ()
  (interactive)
  (insert "O-II~83.4~nm ")
  )
