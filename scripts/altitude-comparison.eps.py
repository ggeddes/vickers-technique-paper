import jsv_rt_cython.multi as vickers_code
import local_pyauric.pyauric as pyauric
import re 
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import time
#from chapman import Chapman, LinearHChapman
from read_redister import frame_data, file_parse
import mcrt

rayleighs = 4*np.pi*1e-6

def chapman_profile(Z0,zKM,H):
    """
    Z0: altitude [km] of intensity peak
    zKM: altitude grid [km]
    H: scale height [km]

    example:
    pz = chapman_profile(110,np.arange(90,200,1),20)
    """
    return exp(.5*(1-(zKM-Z0)/H - exp((Z0-zKM)/H)))

class Chapman:
    def __init__(self,N,H,h):
        self.N = N
        self.H = H
        self.h = h

    def __call__(self,z):
        return self.N * chapman_profile(self.h,z,self.H)

def mcrtAtmosphere(N_angles,neutrals,temperatures,altitudes,oplus):
    """Return a realistic atmosphere for testing purposes"""
    #setup cross sections
    mag = 1e-18
    f = lambda L: [ x * mag for x in L ]
    csO  = f( [  3.89,  3.89,  3.90 ] )
    csO2 = f( [ 33.80, 14.00, 10.40 ] )
    csN2 = f( [ 0.049,  0.29, 10.10 ] )

    amu = 1.660538921e-27 # kg
    mO = 16 * amu
    mO2 = 32 * amu
    mN2 = 28 * amu

    for name, cs, mass in [ ("O", csO, mO ), ("N2", csN2, mN2 ), ("O2", csO2, mO2 ) ]:
        mcrt.Gas( name, 'absorption', [832, 833, 834], cs, mass )

    wavelengths = [ 832.757e-8, 833.329e-8, 834.466e-8 ]
    # mO16 = 1.489917e10 # eV / c^2
    # #e^2 = hbar c alpha
    # hbarc = 1.97327e-5 # eV cm
    # alpha = 1.0/127.0
    # me = 5.109989e5    # eV / c^2
    # c = 2.998e10       # cm / s
    # kb = 8.6173e-5     # eV / K
    # T = 1000           # K

    # sig0_over_f = lambda x : x * np.sqrt( mO16 * hbarc * alpha / ( 2 * kb * T * c * me ) )

    # fosc = [ 0.0459, 0.0916, 0.1371 ]

    sigma_scatter = [5.61E-14, 1.12E-13, 1.68E-13] # from Vickers' thesis, Table 4.2, p. 101
    # sigma_scatter = [ sig0_over_f(w)*fosc[i] for (i, w) in enumerate(wavelengths) ]

    mcrt.Gas( "O+", 'scattering', [832, 833, 834], sigma_scatter, mO )

    # # setup and read AURIC stuff
    # auric = pyauric.AURICManager(data_folder)

    n_species = [ "[O]", "[O2]", "[N2]" ]

    # neutrals = auric.retrieve("atmos.dat", n_species + ['Tn (K)'])
    # temperatures = neutrals['Tn (K)'] 
    # temperatures = temperatures * 1000.0 / np.max(temperatures)
    # absorbers = { k[1:-1]:v for (k,v) in neutrals.items() if '[' in k }
    absorbers = { k[1:-1]:np.asarray(neutrals[k]) for k in neutrals.columns if '[' in k}
    
    # altitudes = neutrals["ALT"]#np.logspace(0,3,len(neutrals["ALT"]))[::-1]

    # # O+ setup
    # oplus = auric.retrieve("ionos.dat", ['[e-]'])

    # two streams:
    angles = np.linspace(0,np.pi,num=N_angles, endpoint=True)

    viewing_angles = np.arange(90-45, 90+45)

    atmosphere = mcrt.Atmosphere( altitudes, angles, temperatures, absorbers, oplus, viewing_angles )

    return atmosphere

def interpolate_data_frame(df,new_index,method='linear'):
    """interpolate a data frame to new_index"""
    all_alts = np.unique(np.hstack([new_index  
                                    , df.index]))
    all_alts.sort()# sort inplace
    interpolated = df.reindex(all_alts).interpolate(method=method)
    interpolated = interpolated.loc[new_index]
    return interpolated

def interpolate_log_data_frame(df,new_index,method='linear'):
    """interpolate the log of a data frame to new_index"""
    all_alts = np.unique(np.hstack([new_index  
                                    , df.index]))
    all_alts.sort()# sort inplace
    log_df = df.apply(np.log)
    interpolated = log_df.reindex(all_alts).interpolate(method=method)
    interpolated = interpolated.apply(np.exp)
    interpolated = interpolated.loc[new_index]
    return interpolated

def main(filename):
    redister_fname = "redister/paper.out"
    with open(redister_fname) as f:
        data = frame_data(file_parse(f))
    # auric = pyauric.AURICManager('.')
    # # read AURIC files
    # dayglo_ver = auric.load('dayglo.ver')
    # dayglo_int = auric.load('dayglo.int')
    # atmos_dat = auric.load('atmos.dat')
    # ionos_dat = auric.load('ionos.dat')
    # # pare them down to the bits we want
    # rx = re.compile('83[234] A') # match "83x A"
    # oii83x_ver = dayglo_ver[[c for c in dayglo_ver.columns if rx.search(c)]]
    oii83x_ver = data['final_source']['sum']
    # oii83x_int = dayglo_int[[c for c in dayglo_int.columns if rx.search(c)]]
    # electrons = ionos_dat['[e-]']
    # neutrals = atmos_dat[
    neutrals = data['atmosphere'][['[O]','[N2]','[O2]']].copy()
    # # add together the photoionization and electron impact contributions
    # emission = pd.DataFrame(index=dayglo_ver.index)
    # for n in ('2','3','4'):
    #     line = '83'+n+' A'
    #     e_impact = 'O+e '+line+' (initial)'
    #     photoion = 'O+hv '+line+' (initial)'
    #     emission[line] = oii83x_ver[photoion]+\
    #                          oii83x_ver[e_impact]
    emission=data['initial_source']['sum']
    # # prepare the inputs to the RT module
    shape = (91,9)
    in_array = np.ndarray(shape,order='C')
    out_array = np.ndarray((180,),order='C')
    # # interpolate to the altitudes that vickers' code likes
    in_alts = np.linspace(100,1000,num=91)
    # neutrals = interpolate_log_data_frame(neutrals,in_alts,method='cubic')
    neutrals.loc[1e8] = 0
    # emission = interpolate_data_frame(emission,in_alts,method='cubic')
    emission.loc[1e8] = 0
    # # combine the relevant data into a single data frame
    # model_inputs = pd.merge(neutrals,emission,left_index=True,right_index=True)
    # # model_inputs['[O+]'] = interpolate_data_frame(electrons,in_alts)
    model_inputs = neutrals.copy()
    model_inputs['[O+]'] = data['atmosphere']['[O+]'].copy()
    # # chapman layer O+
    # chapman_oplus = Chapman(N=1.2e5,T=60,h=320)(in_alts) 
    # # chapman_oplus = LinearHChapman(N=1.2e5,Tbot=100,Ttop=10,h=350)(in_alts)
    # model_inputs['[O+]'] = chapman_oplus
    # # model_inputs['[O+]'].loc[model_inputs.index<20] = model_inputs.index[model_inputs.index<200]*1e-10
    # #model_inputs['TO+ (K)'] = np.ones_like(model_inputs['[O+]'])*500
    # model_inputs['TO+ (K)'] = interpolate_data_frame(atmos_dat['Tn (K)'],in_alts)
    model_inputs['TO+ (K)'] = data['atmosphere']['T'].copy()
    # interpolated = model_inputs.copy()
    
    # plt.plot(interpolated.loc[in_alts])
    # plt.plot(interpolated.loc[model_inputs.index]) 
    # plt.plot(interpolate_data_frame(electrons,in_alts).loc[in_alts],ls=':',label='[e-]')
    # plt.yscale('log')
    # plt.savefig('foo.eps')
    model_inputs['Altitudes (km)'] = data['atmosphere']['Z'].copy()
    model_inputs['832 A'] = emission.copy()*1e-5
    model_inputs['833 A'] = emission.copy()*1e-5
    model_inputs['834 A'] = emission.copy()
    model_inputs.loc[1e8] = model_inputs.loc[model_inputs.index[-1]]*0.99
    model_inputs['Altitudes (km)'][1e8] = 1e8
    model_inputs['Altitudes (km)'] *= 1e-5
    model_inputs = model_inputs.sort_index(ascending=True)
    
    in_labels = ['Altitudes (km)','TO+ (K)','[O]','[N2]','[O2]','[O+]'
                 ,'832 A','833 A','834 A']
    # in_array[:,0] = interpolated.index
    in_array[:,0] = model_inputs['Altitudes (km)'] #This needs to be in km, not cm!
    for col in range(1,shape[1]):
        in_array[:,col] = model_inputs[in_labels[col]]
        # in_array[:,col] = interpolated[in_labels[col]]
    # run the RT calculation
    #vickers_code.runRT_func(in_array,out_array)

    # run mcrt as well

    # # O+ falloff
    # model_inputs['[O+]'] *= np.where(model_inputs['Altitudes (km)'] > 600 , 1e-3,1)
    arraycopy = lambda x: np.asarray(x).copy()
    atm = mcrtAtmosphere(N_angles = 6
                         , neutrals = model_inputs[['[O]','[N2]','[O2]']].copy()
                         , temperatures = arraycopy(model_inputs['TO+ (K)'])
                         , altitudes = arraycopy(model_inputs['Altitudes (km)'])
                         , oplus = arraycopy(model_inputs['[O+]'])
    )
    # Q = atm.single_scatter_matrix(2)
    # plt.imshow(Q)
    # plt.savefig("Q.png")
    # plt.close()
    Q = np.load('Q_100.npy')
    I = np.eye(Q.shape[0])
    M = np.linalg.inv(I-Q)
    print("Multiple scattering matrix shape:",M.shape)
    # weights = np.ones(atm.N_angles)/atm.N_angles
    weights = np.polynomial.legendre.leggauss(atm.N_angles)[1]/2
    src = np.asarray(model_inputs['834 A'].copy())[:-1]
    print(src.shape)
    P0 = np.hstack([_[0]*_[1] for _ in zip([src]*atm.N_angles,weights)])
    P1 = P0.dot(Q)
    P2 = P1.dot(Q)
    P3 = P2.dot(Q)
    PF = P0.dot(M)
    PF = PF.reshape(-1,src.shape[0]).sum(0)
    P1 = P1.reshape(-1,src.shape[0]).sum(0)
    P2 = P2.reshape(-1,src.shape[0]).sum(0)
    P3 = P3.reshape(-1,src.shape[0]).sum(0)


    fig = plt.figure()
    ax = plt.gca()
    # # model_inputs.plot(ax=ax)
    # # ax.set_yscale('log')
    # za = oii83x_int.index
    # auric_limb = sum(oii83x_int['83'+x+' A'] for x in '4')# '234')
    # #oii83x_int.plot(ax=ax)
    # ax.plot(auric_limb,za,label="AURIC Limb")

    # ax.plot(out_array*rayleighs,range(1,181),label='MC Limb')
    # ax.set_xlabel("Intensity (R)")
    # ax.set_ylabel("Zenith Angle (deg)")
    # ax.set_title("Model Limb Intensity")
    # ax.set_ylim(90,120)
    # ax.legend()
    # a,b = oii83x_int.index.min(), oii83x_int.index.max()
    # ax.set_ylim(a,b)
    # ax.set_xlim(1e-6,)
    ax.invert_yaxis()
    fig.savefig('../figures/foo.eps')
    fig = plt.figure()
    ax = fig.gca()
    model_inputs[r'834 A final'] = data['final_source']['sum']
    # model_inputs[['834 A','834 A final']].plot(ax=ax,legend=True)
    Z = model_inputs.index * 1e-5
    z = np.asarray(model_inputs.index)[:-1] * 1e-5
    # ax.plot(z,P0.reshape(-1,z.shape[0]).sum(0),label='mcrt input',ls='--')
    ax.plot(model_inputs['834 A'],Z,label='Initial source',color='k',ls=':')
    ax.plot(model_inputs['834 A final'],Z,label='Iterated Feautrier',color='k')
    ax.plot(PF,z,label='Markov chain',color='k',ls='--')
    # ax.plot(z,P1,label='P1')
    # ax.plot(z,P2,label='P2')
    # ax.plot(z,P3,label='P3')
    # ax.plot(atm.oplus/1000,atm.z)
    # ax.plot(model_inputs.index,model_inputs['[O+]'])
    ax.set_title(r"Resonant Scattering Model Comparison")
    ax.set_xlabel(r"Volume Emission Rate [cm$^{-3}$ s$^{-1}$]")
    ax.set_ylabel(r"Altitude [km]")
    ax.legend(loc='best')
    ax.set_xlim(1,1e3)
    ax.set_xscale('log')
    ax.set_ylim(100,1000)
    fig.savefig(filename)
    plt.close()

    fig2 = plt.figure()
    ax2 = fig2.gca()
    ax2.plot(atm.extinction[2])
    ax2.set_yscale('log')
    fig2.savefig('../figures/foo.eps')
    return fig

main("/home/george/Documents/vickers-technique-paper/figures/altitude-comparison.eps");quit()

if __name__ == "__main__":
    import os, sys
    # cut off the .py extension from the currentscript name
    basename = os.path.basename(__file__)[:-3]
    dirname  = os.path.dirname(__file__)
    # filename = scripts/../figures/basename
    filename = os.path.join(dirname,os.path.pardir,"figures",basename)
    filename = os.path.abspath(filename)
    print("generating",filename+"... ",end='')
    sys.stdout.flush()
    fig = main(filename)
    print("done.")
