import matplotlib.pyplot as plt

def main(filename):
    fig = plt.figure()
    #fig.savefig(filename)
    plt.close()
    return fig

if __name__ == "__main__":
    import os, sys
    # cut off the .py extension from the currentscript name
    basename = os.path.basename(__file__)[:-3]
    dirname  = os.path.join(os.path.dirname(os.path.dirname(__file__)),"figures")
    filename = os.path.join(dirname,basename)
    print("generating",filename+"... ",end='')
    sys.stdout.flush()
    fig = main(filename)
    print("done.")
