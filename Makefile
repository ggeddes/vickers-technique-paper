TEX = pdflatex -interaction=nonstopmode -file-line-error
BIB = bibtex
PYTHON = ~/anaconda3/bin/python
TIKZ=pdflatex -shell-escape -interaction=nonstopmode -file-line-error "\input"
OUTPUTDEVICE=&1
FORMAT_MODULE = scripts/aguformat
VIEW = see
VPATH=text/:scripts/:figures/:scripts/jsv_rt_cython/RT
BIBFILES= UV.bib OII834.bib

.PHONY : view clean figures submission diff-highlight

all : final.pdf

view : final.pdf 
	$(VIEW) $<

clean :
	rm main.pdf *.aux *.bbl *.blg *.log final.tex
	rm figures/*-figure0.pdf submission/*

submission: final.pdf final.tex figures
	mkdir -p submission
	cp final.tex submission/final.tex
	cp figures/step-diagram.eps submission/fig1.eps
	cp figures/tau-grid.eps submission/fig2.eps
	cp figures/altitude-comparison.eps submission/fig3.eps
	cp figures/random_100_models_mar10_scale1_25.pdf submission/fig4.pdf
	cp figures/cornerFriJan151400002010_scale1_7.pdf submission/fig5.pdf
	cp figures/cornerWedMar101643002010_scale1_25.pdf submission/fig6.pdf

diff-highlight: final.submission.tex final.tex
	latexdiff $^ > changes.tex 
	$(TEX) -output-directory review changes.tex

figures: figures/tau-grid.eps figures/step-diagram.eps
	rm figures/step-diagram.pdf
	rm figures/tau-grid.pdf

final.pdf : final.tex final.aux
	$(TEX) $< >$(OUTPUTDEVICE)

final.aux : final.tex main.aux
	cp main.aux final.aux

final.tex : main.tex main.aux main.bbl main.blg
	$(TEX) $< >$(OUTPUTDEVICE)
	python2 -m $(FORMAT_MODULE) main final 

main.bbl main.blg : main.aux $(BIBFILES)
	$(BIB) main >$(OUTPUTDEVICE)

main.aux : main.tex figures text/*.tex
	$(TEX) $< >$(OUTPUTDEVICE)

figures/step-diagram-figure0.pdf: step-diagram.tex
	cd figures && $(TIKZ) step-diagram.tex >$(OUTPUTDEVICE)

figures/step-diagram.eps: figures/step-diagram-figure0.pdf
	pdftops -eps $< $@

figures/tau-grid-figure0.pdf: tau-grid.tex
	cd figures && $(TIKZ) tau-grid.tex >$(OUTPUTDEVICE)

figures/tau-grid.eps: figures/tau-grid-figure0.pdf
	pdftops -eps $< $@

figures/%.eps: scripts/%.eps.py 
	cd scripts && $(PYTHON) $(notdir $<)
.PHONY: figures/atlitude-comparison.eps

scripts/altitude-comparison.eps.py: multi.so scripts/redister/834.out
.PHONY: scripts/altitude-comparison.eps.py

scripts/redister/834.out: scripts/redister/Makefile scripts/redister/inputs/834.text
	make -Cscripts/redister 834.out

multi.so: multi.c _multi.pyx
	cd scripts/jsv_rt_cython && $(PYTHON) setup.py build_ext --inplace
	cp `find scripts/jsv_rt_cython/multi.*.so` scripts/jsv_rt_cython/multi.so
