%% I don't think this is the right place for this, maybe in a different section?
% The \citet{douglas_evaluation_2012} study comparing an NRL forward model to data for two limb profiles implied a causal relation between the ionospheric profile and the ionospheric plasma density. Thus, within a constant scaling factor, the forward model accurately described a probable state of the system.
% This forward model comparison, while useful, failed to measure the information content of our observation, merely illustrating the correlation between our model and observation.
% To assess the information content of the example observations shown previously by \citet{douglas_evaluation_2012} we used the \texttt{emcee} affine-invariant {MCMC} sampler \citep{foreman-mackey_emcee:_2013}. 

\section{Inverse Model}
\label{section:inversion}

% This feels unsatisfying as an explanation without mentioning the detailed balance condition.

%\subsection{Inverse Model}% section title feels like it needs work...

%% Errors are Gaussian if the photon counts are high (by the central limit theorem)
%% If the limb is dim though, there will be non-Gaussian shot noise
% Since the limb profile measurements are expected to be dominated by poisson noise, 
In order to minimize overfitting, we chose a simple constant scale height Chapman\(-\alpha\) function to model the \(\mathrm{O}^+\) density profile,

\begin{equation}
  \label{equation:chapman} 
  n(z|N_m,h_m,H_m) = N_m\sqrt{\exp\left[1-\frac{z-h_m}{H_m}-\exp\left(-\frac{z-h_m}{H_m}\right)\right]},
\end{equation}

\noindent{}where \(N_m\) is the density at the peak of the ionosphere, \(h_m\) is the height of the peak of the ionosphere, and \(H_m\) is the scale height. This is a useful starting point, as it has been shown that this simple model of the ionosphere sufficiently approximates the height of the F2 peak for OII~83.4~nm modeling~\citep{douglas2012}. Models with additional parameters can yield good retrieval results as long as the parameters are nondegenerate, and additional parameterizations will be the topic of future work. 

In order to use the Markov chain forward model of section~\ref{section:scattering-model} we must supply an initial source of 83.4 nm photons.
The primary source of these is from excited \(\mathrm{O}^+\) ions decaying to the ground state.
Ions are excited into this state by solar photoionization, secondary electron impact ionization, and dissociative ionization of \(\mathrm{O}_2\).
There is also a negligible contribution from scattered solar photons~\citep{meier1991}.
The combined contribution of scattered solar flux, secondary electrons, and dissociative ionization is less than 10 percent of the total emission source, so we will consider only the \(\mathrm{O}^+(^4S)\leftarrow\mathrm{O}^+(^4P)\) photoemission rate in our initial source contribution~\citep{vickers1996}.
%\(\mathrm{O}^+(^4S)\mathrm{O}^+(^4P)\)
We compute the photoemission rate using the \emph{Atmospheric Ultraviolet Irradiance Code} (\texttt{AURIC})~\citep{AURIC} to simulate a scattered limb intensity profile using the forward model described in section~\ref{section:scattering-model}. 
\texttt{AURIC} employs NRLMSISE-90 and a modified version of the Hinteregger solar flux model to compute the initial photoproduction rate of O-II~83.4~nm and other airglow features.
\citet{emmert2010} found that density of O during the 2007-2009 solar minimum was as much as 12\% below values predicted by MSIS, while other species were as much as 3\% below expected values. 
Errors in the modeled neutral atmosphere will of course affect the initial photoproduction rate as well. %This just kind of hangs here....
We denote the simulated limb intensity profile corresponding to the chapman parameters \(\{N_m,h_m,H_m\}\) as \(\hat{y}(\theta | N_m,H_m,h_m)\).

The limb intensity data from {RAIDS EUVS} are a vector of intensities $\vec y$ and look directions \(\vec \theta\).
%A RAIDS EUVS limb profile is integrated over approximately 3 minutes at a sensitivity of 0.5 counts per second per Rayleigh. 
The RAIDS EUVS scans across the limb to collect data with the tangent point altitudes ranging from 100~km to 300~km.
The data have been co-added over a 3 minute period and collected into 11 altitude bins to increase the signal to noise ratio (SNR).
The integration time in each altitude bin is therefore  180 s/11 \(\approx\) 16 s. 
The O-II~83.4~nm emission is of order 500 Rayleighs, giving a SNR of \(\sqrt{16\times0.5\times500} \approx 63\), so we assume measurement errors follow a Gaussian distribution. % This depends mostly on the detector and the desired time resolution 
Given a proposed set of Chapman parameters $\{N_m,H_m,h_m\}$ we compute the likelihood that the observed data were obtained from the corresponding emission predicted by our forward model
\citep{vanderplas_frequentism_2014},

\begin{equation}
  P(\vec y| N_m,H_m,h_m )=\prod_{i=1}^N \frac{1}{\sqrt{2\pi\sigma_i^2}}\exp{\left[-\frac{(y_i-\hat{y}(\theta_i | N_m,H_m,h_m))^2}{2\sigma_i^2}\right]}.
  \label{eq:likelihood}
\end{equation}


% This framework lends itself to random sampling of the Chapman parameters ($N_M,H_m,$ and $ h_m$) via {MCMC} -- assessing  the underlying probability distribution via many model realizations. 
% \texttt{emcee} expects a log-likelihood, which we find by taking the natural logarithm of Eq. \ref{eq:likelihood},
The retrieved parameters are those which maximize the likelihood of obtaining the observed data. 
% It is also convenient to take the natural logarithm of Equation~(\ref{eq:likelihood}) to change the product to a sum which is quicker to compute:

% \begin{equation}
%   \ln\left(P(\vec y| N_m,H_m,h_m )\right)=-\frac{1}{2} \sum_i^N \left(\ln (2\pi\sigma_i^2)+\frac{[y_i-\hat{y}(\theta_i | N_m,H_m,h_m)]^2}{\sigma_i^2}\right).
%   \label{eq:loglikelihood}
% \end{equation}
In order to find the maximum likelihood parameters, we need to thoroughly sample the space of possible input parameters.
The {DIT} approach of \citet{picone1997} is a robust method to invert a physical system when uncertainties in model parameters and measurements are both approximately Gaussian. 
However, real observational uncertainties are not exactly Gaussian, particularly at high altitudes where the limb is not bright and photon counts are Poisson-distributed.
Model parameters retrieved from Gaussian observations will not have Gaussian errors since the parameters are simply linear functions of observed limb intensity.
In this case, {DIT} or the iterative methods used by \citet{vickers1996} may converge on a local minimum or fail to converge at all. 
Monte Carlo techniques randomly sample a parameter space in a way that is stable to anomalies and local minima \citep{sambridge_monte_2002}. 

Our model does propagate \(\mathrm{O}^+\) density parameters non-linearly, so we chose to sample the Chapman parameter space using a Markov chain Monte Carlo (MCMC) technique. 
Since each radiative transfer calculation is computationally intensive, a short algorithm convergence time minimizing the number of forward model runs is important for future efforts to parameterize the ionosphere in real time via 83.4 nm observation.
\citet{goodman_ensemble_2010} described an affine-invariant sampler which converges more rapidly than the more common Metropolis-Hastings {MCMC} method for skewed datasets where the likelihood is asymmetrical in parameter space which is used by the python package \texttt{emcee}~\citep{emcee}.
We used \texttt{emcee} to sample the Chapman parameter space for the retrieval of \(O^+\) density.
While faster convergence could be expected with a prior based on solar conditions or ground-based measurements, we begin with the na\"ive case of a uniform prior of Chapman parameters in the region \([20<H<60,200<h<300,7<\log_{10}N<11]\). 
% The convergence time can be seen in Fig. \ref{fig:MCMC_burnin} where the value of the Chapman-$\alpha$ parameters are shown for each walker as a function of step number. 

\section{Results}
\label{section:results}
We have tested our retrieval algorithm by applying it to RAIDS EUVS O-II~83.4~nm limb data.
We compare the retrieval results to a coincident \(\mathrm{O}^+\) density measurement from the Millstone Hill incoherent scatter radar (ISR).
Although the ISR measures electron density rather than \(\mathrm{O}^+\) density, \(\mathrm{O}^+\) ions dominate the ionosphere between 200~km and 600~km, so there should be good agreement between the two.
In addition, the ISR measurement is associated with a fixed location while the RAIDS EUVS limb is integrated over about 1500~km of orbit, covering about 10 degrees of latitude.
The set of tangent points included in each of the two limb intensity profiles we use here is shown in Figure~1 of \citet{douglas2012}.
It is important to note that observed 83.4 nm limb intensity originates from a point closer to the observer than the tangent point of the line of sight, so information retrieved from such a measurement is about that point rather than the tangent point itself.
However the region with which the retrieval is best associated will depend on the retrieved parameters, so we will continue to associate retrievals with tangent point for convenience. 
The \texttt{emcee} configuration used for retrieval is shown in Table~\ref{table:emcee}. 
The configuration thoroughly sampled the parameter space with 26,400 forward models (48 walks of 550 steps each).
The total runtime was limited to 15 minutes due to system constraints.
%The time to convergence or ``burn-in''  is shown as a vertical dashed line, values preceding the ``burn-in'' time do not reflect the underlying distribution and were excluded from subsequent analysis.
%% This refers to a missing figure. I don't think we need to talk about the burn-in in this paper. Maybe we do?
Figure \ref{fig:10mar_100random} shows 100 randomly drawn forward models after the burn-in time, showing each realization is a qualitatively plausible fit  at the level of the forward models presented in \cite{douglas2012}.
Figures \ref{fig:15janMCMC} and \ref{fig:10marMCMC} show the posterior probability distributions for the same two observations as forward modeled from the Millstone Hill radar. 
In these ``corner'' plots \citep{foreman-mackey_triangle.py_2014} each histogram represents the marginal distribution of a single retrieved Chapman parameter.
Each two-dimensional plot represents the joint distribution of two Chapman parameters.
% This next sentence is important, but I have no clue how to say it properly. Also, should it go to the section before this one?
Although the prior distribution is uniform for each parameter, the retrieved (posterior) distributions are informed by observed data and measure confidence that a particular Chapman ionosphere could produce the observed scattered limb emission.
The ground-truth Millstone Hill {ISR} measurements of the corresponding Chapman$-\alpha$  parameters are shown as vertical lines in each histogram and intersecting lines in the density plots. 
Since there is a large uncertainty in the instrument calibration, %(see \ref{sec:chap2calibration}) -- what do we do about this one for this paper?
the magnitude $S_0$ has been adjusted by an altitude constant scale factor such that peak of the retrieved \(N_m\) matches the ground-truth measurement.
(While similar and analogous to rescaling the final profile in the forward model comparison, adjusting $S_0$ rather than the final profile is biased toward correction of the volume excitation rate than the instrument calibration).
In the figures shown,  the multiplicative scaling factor for the 15 January, 2010 observation is 1.7$\times$ and on 1.25$\times$ 10 March, 2010.
While manual rescaling prohibits the single dataset retrieval of all three Chapman parameters without an external calibration, it shows that \(N_m\) can be retrieved given \(h_m\) or an external calibration.
Thus, this observation provides information constraining the ionosphere to a range of scale heights and peak altitudes.

While a degeneracy between peak height and peak density  can clearly be observed (first column, middle row) there is a well localized region of high probability slightly offset from the radar parameter peak for the 15 January 2010 dataset, while the scale-height (bottom row) is poorly constrained  but peaked near the true value.
There are only a small number of close overflights of RAIDS coincident with ISR measurements, so it is not possible to definitively determine whether the distinct offset between retrieved values and ground based measurements is due to differences in the ionospheric volume space sampled by the {ISR} and {RAIDS EUVS} observations or deficiencies in the forward model.
The more uniformly bright limb profile on 10 March 2010 provides less constraint on the probable scale height, implying the topside morphology (the decline in intensity at high altitudes visible on 15 Jan) drives the information content of the observation. 
An instrument which measures O-II~83.4~nm emission at a wider range of tangent-point altitudes, either by observing from a higher altitude than the {ISS} \citep{mccoy1985} %\cite{geddes_error_2015}
or by slowly rotating (e.g.  \cite{cotton_tomographic_2000}) would better constrain the morphology and narrow the retrieved parameter probabilities. 
%However, the {ISS} provides a flexible and convenient platform for atmospheric studies and future missions are expected to share the {RAIDS} observing geometry.
Thus, another means of breaking the degeneracy between Chapman parameters is required.

% Assuming a Gaussian uncertainty in the measurements.
% from inversion paper draft:
\subsection{Other data sources} 
%This section describes how lessons learned from the preceding studies of 83.4 nm limb profiles inform future work to better constrain ionospheric density profiles.
Previous studies of 83.4 nm emission have emphasized  the ``uniqueness'' of the ionospheric density  retrieved in the narrow sense of whether a single ``best-fit'' exists and closely approximates the expected ionosphere.
The best-fit approach breaks down for noisy data such as the presented RAIDS EUV profiles.
The results of the preceding {MCMC} retrieval analysis of RAIDS EUVS data show that additional information is required to constrain the retrieved ionosphere to the underlying physical parameters when observation uncertainty is too large. 
A Bayesian approach permits integrative analysis of the ionosphere, maximizing the leverage of even a poor measurement via straightforward inclusion of additional data. 
An auxiliary data source could improve the retrieval quality significantly given an appropriate prior distribution.
For example, a measurement that fixes either \(N_m\) or \(h_m\) would break the degeneracy between those parameters and allow the other to be retrieved accurately as well.
For example the peak density and peak height could be compared to results from the digisonde network of the Global Ionospheric Radio Observatory (GIRO).
Total electron content from GPS are available as well and would break the \(N_m,h_m\) degeneracy as well since they are proportional to the peak density but not the peak altitude. 
%External data sources could provide electron density priors, such as global ionospheric models (e.g. \cite{schunk_global_2004%,galkin_global_2014}) or total electron content measurements from GPS \citep{rideout_automated_2006}.% Can't find the reference for galkin_global_2004 

\subsection{Future Observations}
Retrievals informed by external data sources suffer from mismatches in calibration, thus there is also an incentive to increase the information recovered from a single EUV measurement.
The absolute calibration of the RAIDS EUVS is of order twenty-percent \citep{stephan_remote_2009,christensen_instrumentation_1992} while MSIS neutral oxygen densities have uncertainties exceeding twenty-percent. % \citep{bennett_alternative_2001}. -- couldn't find this one
This uncertainty, coupled with uncertainty in the solar irradiance explains the difficulty in determining the source function intensity.
As presented previously, forward modeling of {RAIDS} data found a calibration error of up to 21\% \citep{douglas2012}. 

The RAIDS EUVS is mounted on a nodding platform with a  slit that has a \(2.4^\circ\) (horizontal) by \(0.1^\circ\) (vertical) field of view. This slit scans across the limb to sweep out a \(16.5^\circ\) field of regard. The soon-to-be-launched {LITES} employs a toroidal spectrograph and a \(10^\circ\times 10^\circ\) field of view to facilitate continuous spectral observation of multiple zenith angles \citep{cotton_single-element_1994}. 
The larger field of view enables LITES to observe all altitudes simultaneously, with a variable exposure time.  %LITES also has a detector which is more sensitive at 83.4~nm. %\citep{geddes_error_2015}. 
Furthermore, the LITES detector has a sensitivity of 15 counts per second per Rayleigh, 30 times higher than RAIDS EUVS~\citep{LITES-SPIE,RAIDS}.
Taking into account the different fields of view, the sensitivity along a particular look direction is 50\% higher in LITES than RAIDS EUVS, which will put tighter constraints on retrieved parameters.
% Assuming photon noise dominates the uncertainty of both {RAIDS} and {LITES}, the signal to noise ratio (SNR) will improve by a factor of \(\sqrt{15} \approx 4 \). 
%%%%%%%%%%%%%%%% This needs to be redone
%Each Chapman parameter is well localized in probability density, aside from the extended degeneracy between parameters seen in Fig. \ref{fig:15janMCMC} and the offset between retrieved values and ground based measurements mirrors that of the original retrieval.

The co-adding of RAIDS EUVS measurements to generate profiles requires including measurements over a wide geographic range. 
Thus, alternatively, the increased {LITES} signal allows binning observations to SNR equal to that of {RAIDS EUVS} for increased spatial resolution.
A {LITES} measurement of the same signal-to-noise as these RAIDS EUVS observation would cover a much shorter ground track and corresponding ionospheric volume, reducing the variability in measured emission rate due to small and medium scale structures such as traveling ionospheric disturbances and the equatorial ionization anomaly.
% spread-F ``bubbles'' \citep{christensen_initial_2003}. % This is about 1356 Å nightglow

The increased precision will tightly constrain the morphology, suggesting value in attempting to retrieve alternate parameterizations of the \(\mathrm{O}^+\) density, such as a linearly changing scale height Chapman layer model.
There is still an uncertainty associated with the initial production of the 83.4 nm emission.
The production source will be measured indirectly by observing the optically thin OII~61.7~nm feature~\citep{stephan2012}.
OII~61.7~nm shares the same production mechanism as OII~83.4~nm, but it is not scattered at all.
This feature is not as bright as O-II~83.4~nm, and the assumption of Gaussian observation uncertainty may not hold. 
Making use of this measurement in the Bayesian retrieval method is possible, but will require a model describing the exact ratio of the two emission rates.
Including an observation of OII~61.7~nm is expected to improve accuracy by eliminating the dependence on solar EUV flux and neutral density models, but further work is required to confirm and quantify this improvement.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
