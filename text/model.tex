%!TEX root = ../main.tex

\section{Forward Model}
\label{section:scattering-model}

The radiative transport equation can be expressed as an integral equation which is often difficult to solve numerically. % cite % ref equation here?
A common approach to its solution originates from \citet{feautrier1964} and is centered around a change of variables based on the two-stream approximation. % cite Feautrier

Photons in the O-II~83.4~nm triplet may be resonantly scattered by ground state \(\mathrm{O}^+\) ions as they travel through the ionosphere. 
The path taken by a photon in a scattering medium is a random walk and a Markov chain is a matrix containing the probabilities for each possible step~\citep{esposito1978}.
A photon's state is described by its direction of travel \((\hat n)\), its frequency \((\nu)\), and the location of the photon \((\vec r)\).  We will ignore polarization effects.
A step in the Markov chain is a transition from one state \((\vec r,\hat n,\nu)\) to another \((\vec r',\hat n',\nu')\), illustrated in Figure~\ref{figure:step-diagram}.
Given a photon in one state, it will make a transition to another state with some probability, \(P\) (Equation 1).
\(P\) is the product of \(P_T\), the transmission probability from \(\vec r\) to \(\vec r'\) and \(P_S\), the probability to scatter at the end of the transmission path.
The probabilities corresponding to each possible step from one state to another are the elements of the Markov chain matrix. % Needs work here.

\begin{equation}
  \label{step-prob}
  P\left(\vec r', \hat n', \nu' | \vec r, \hat n, \nu \right) = 
  P_T(\vec r'| \vec r,\hat n, \nu) P_S(\hat n',\nu'|\vec r', \hat n, \nu).
\end{equation}

The photon originating from \(\vec r\) will be absorbed or scattered at \(\vec r'\) with probability 

\begin{equation}
  P_T = e^{-\tau(\vec r,\vec r',\nu)},
\end{equation}

\noindent{}where \(\tau(\vec r,\vec r',\nu)\) is the optical depth along the path (Equation~\ref{optical-depth}), \(\hat n\) is a unit vector along the path (Equation~\ref{unit-vector}), and where \(\kappa(\vec r,\nu)\) is the extinction coefficient (Equation~\ref{ext-coeff}),

\begin{equation}
  \tau(\vec r,\vec r',\nu) = \int_0^{\left|\vec r' - \vec r\right|} \kappa(\vec r + \hat n s,\nu) \, d s
  \label{optical-depth}
\end{equation}
\begin{equation}
  \hat n = \frac{\vec{r}' - \vec{r}}{\left|\vec r' - \vec r\right|}
  \label{unit-vector}
\end{equation}
\begin{equation}
  \kappa(\vec r,\nu) = \sum_{\mathrm{species X}}
  % _{ \mathrm{X} \in \{\mathrm O_2,\mathrm N_2,\mathrm O,\mathrm{O}^+ \} }
  [\mathrm{X}](z)\sigma_\mathrm{X}(\nu)
  \label{ext-coeff}
\end{equation}

The probability that the event that occurs at \(\vec r'\) is scattering rather than absorption is the albedo, \(\varpi(\vec r',\nu) = \frac{\kappa_{s}(\vec r', \nu)}{\kappa_{s}(\vec r', \nu) + \kappa(\vec r', \nu)}\).
The scattering probability \(P_s\) is the product of the albedo and some redistribution function, \(R(\hat n, \hat n', \nu, \nu', \vec r')\).
In general, the redistribution function can couple almost all of the variables together, making the integration difficult.
In the following sections, we will simplify the redistribution function by introducing the assumptions of complete frequency redistribution and a plane-parallel, azimuthally symmetric geometry. 

% When the primed and unprimed variables are integrated over disjoint regions of the atmosphere, the transmission probability density, \(P_T\) breaks up further.  
% % need figure to depict regions
% Now the transmission probability the product of the probability to escape region I and the probability to be transmitted to region II without absorption or scattering. 



% %subsection here?
% The probability that a photon emitted at \(\vec r\) will be transmitted without absorption or scattering will be transmitted to \(\vec r'\) is 

% \begin{equation}
%   \label{transmission-prob}
%   P_T(\vec r'|\vec r, \nu) = \exp\left[-\tau(\vec r - \vec r',\nu)\right],
% \end{equation}

% where \(\tau\) is the optical depth along the path and \(\nu\) is the frequency.
% As the photon arrives at \(\vec r'\) and passes along its path to \(\vec r' + \delta r\), it may be scattered in some new direction and frequency by an ion moving with some thermal velocity.
% The probability for this single scattering event is 

% \begin{equation}
%   \label{scattering-prob}
%   P_S(\vec r', \hat n', \nu',\delta s| \hat n, \nu) = \kappa_s(\vec r',\nu) \delta s R(\vec r',\hat n,\hat n',\nu,\nu'),
% \end{equation}

% where \(\kappa_s = n_{\mathrm{O}^+}\sigma_{\mathrm{O}^+}\) is the scattering coefficient and \(R\) is the redistribution function by which the frequency and direction of the photon may be shifted.
% %Technically both of the above are probability densities, but that is probably just extra clutter.
% The redistribution function for the problem depends on the details of the scattering problem being solved. In the case of O-II~83.4~nm, we will assume complete frequency redistribution with a Voigt lineshape and isotropic scattering.


\subsection{Complete Frequency Redistribution}
\label{section:CFR}
\label{section:lineshape}

Line center optical depths for the O-II~83.4~nm emission in the terrestrial ionosphere are typically of order 1-10. % citation needed
This is  thick enough for scattering to change the emission intensity but small enough to assume the scattering is incoherent. % cite Meier 1991
In this case we make the approximation of complete frequency redistribution,

\begin{equation}
  R = p(\hat n, \hat n') \frac{\phi(a,\nu)}{\sqrt{\pi}\Delta\nu_D}  \frac{\phi(a,\nu')}{\sqrt{\pi}\Delta\nu_D'}, 
\end{equation}

\noindent{}where \(p\) can be chosen as either the Rayleigh phase function or \(\frac{1}{4\pi}\) for isotropic scattering, \(\Delta\nu_D = \sqrt{\frac{8kT\ln 2}{m_{\mathrm{O}}c^2}}\nu_0\) is the Doppler width, and \(\phi\) is the Voigt lineshape function \citep{meier1991}.

The lineshape of the scattering cross section describes how photons which are slightly off of the line center due to Doppler shift will interact with the scattering medium. 
The Voigt function is the convolution of the natural lineshape and a Doppler-broadened Gaussian lineshape. The unnormalized Voigt function is, 

\begin{equation}
  \label{voigt}
  \phi(a,x) = \frac{a}{\pi}\int_{-\infty}^\infty\frac{\exp(-y^2)}{(x-y)^2 + a^2}dy,
\end{equation}

\noindent{}where \(a = \frac{A}{4\pi\Delta\nu_D}\) is the Voigt parameter, \(A\) is the reciprocal of the natural lifetime and \(x = \frac{\nu-\nu_0}{\Delta\nu_D}\)  is a frequency parameter.% \citep{rybicki2008}.

\subsection{Geometry}
\label{section:geometry}

In order to cast the problem into a matrix form, we must divide the continuous space of photon states into discrete bins. 
For the spatial variables \(\vec r, \vec r'\), we chose an azimuthally symmetric, plane-parallel geometry. 
This choice simplifies the numerical implementation of the problem by allowing us to substitute the altitude variable \(z\) for the optical depth from infinity, 

\begin{equation}
  \label{tau-of-z}
  \tau(z,\nu) =  \int^\infty_z n(z)\sigma(T) \phi(a,\nu) dz + \sum_{\mathrm{x}} \int^\infty_z\kappa_\mathrm{x}(z) dz,
  % \mathrm{where}\quad \kappa(z,\nu) &=& \sum_{ \mathrm{X} \in \{\mathrm O_2,\mathrm N_2,\mathrm O,\mathrm{O}^+ \} } [\mathrm{X}](z)\sigma_\mathrm{X}(\nu).
\end{equation}

\noindent{}where \(T\) is the temperature at altitude \(z\)  and \(\kappa_\mathrm{x}\) is the extinction cross section for neutral species x.

We replace the continuous variable \(\tau\) with a discrete set of values \(\{\tau_1,\tau_2...,\tau_n\}\), with \(\tau_{i+1} > \tau_{i}\) and \(\tau_0 = 0 \), as shown in Figure~\ref{figure:tau-grid}. 
%In the case where region I is below region II,  regions I-III can be defined as the intervals, \(\mathrm{I}=[\tau_{i},\tau_{i+1}],\, \mathrm{II}=[\tau_{j+1},\tau_{i}],\, \mathrm{III}=[\tau_{j},\tau_{j+1}]\).
% Now we can write down the optical thicknesses from equation~(\ref{key-prob}) in terms of \(\tau\) and \(\mu\) as,

% \begin{eqnarray}
%   \Delta_{\mathrm{I}}(\tau,\nu,\mu) = \left(\tau-\tau_{i}\right)/\mu, \nonumber \\
%   \Delta_{\mathrm{II}}(\nu,\mu) = \left(\tau_{i} - \tau_{j+1}\right)/\mu,  \nonumber \\ 
%   \Delta_{\mathrm{III}}(\tau',\nu,\mu) = \left({\tau_{j+1}}-{\tau'}\right)/\mu.
% \end{eqnarray}

We will also consider a discrete set of zenith angles \(\{\mu_1,\mu_2...,\mu_m\}\)  at which photons can travel. We employ Legendre-Gauss quadrature to replace the integral over angle with a weighted sum,

\begin{equation}\label{legendre-gauss}
  \int_{-1}^{+1} f(\mu) d\mu \approx \sum_{i=1}^mw_if(\mu_i).
\end{equation}

With the assumptions made thus far, the mean probability of a single scattering event is, 

\begin{equation}
  \label{equation:nasty-thing}
  P_{ij} = \frac{
     \int_{\tau_{i}}^{\tau_i+1} d\tau \int_{\tau_j}^{\tau_{j+1}} d\tau' ww' e^{\left(\tau-\tau_{i}\right)/\mu} e^{\left(\tau_{i} - \tau_{j+1}\right)/\mu} e^{\left({\tau_{j+1}}-{\tau'}\right)/\mu} 
    \varpi(\tau',\nu)
    R(\mu,\mu',\nu,\nu',\tau)
  }
  {\int d\tau \int d\tau'},
\end{equation}

where \(\varpi = \frac{[\mathrm{O}^+]\sigma_{\mathrm{O}^+}}{\sum_{\mathrm{x}}[\mathrm{x}]\sigma_{\mathrm{x}}}\) is the single-scattering albedo. 
In order to integrate equation~(\ref{equation:nasty-thing}), we will assume that the temperature is nearly constant within each optical depth bin. 
% This will allow us to ignore \(R\) when integrating over \(\tau\) and \(\tau'\).
We will also assume \(\varpi(\tau)\) is constant across each bin.
This will allow us to obtain an analytical expression for the average probability contained in a bin.
In practice, simply taking the albedo value from the top or bottom of the bin has little effect on model output.
%Ignoring the rapid change in the middle may effect the amount of scattering there, but will also make the computation simpler.

To evaluate (\ref{equation:nasty-thing}) under these assumptions we use the integral, 

\begin{equation}
  \int_0^{\Delta\tau} e^{\tau} d\tau = (e^{\Delta\tau} - 1).
\end{equation}

The step probability averaged over a set of optical depth bins becomes,

\begin{equation}\label{equation:simplified-step}
  P_{ij} = \frac{ww'}{4}\varpi_jR \frac{\mu^2}{\Delta\tau_i\Delta\tau_j}(e^{-\Delta\tau_i/\mu}-1)(1-e^{-\Delta\tau_j/\mu})e^{-(\tau_j - \tau_i)/\mu},
\end{equation}

\noindent{}where \(\Delta\tau_i = \tau_{i+1} - \tau_i\), \(\varpi_j = \varpi(\tau_j)\), \(R = R(\mu,\mu',\nu,\nu',\tau)\), and \(w\) is the Gauss-Legendre weight corresponding to the angle \(\mu\).

% % Defining states:
% We will label each possible combination of values for the variables with an index and refer to this set of values as a state of the Markov chain. 
% Explicitly, each state consists of a region of origin around \(\vec r_i\), a frequency range about \(\nu_i\) and a direction in some solid angle about \(\hat n_i\).

% Here we will assume a plane parallel, azimuthally symmetric atmosphere, so that the spatial bins are simply a set of altitudes \(\{z_i\}\) and the optical depth between two points \(\vec r_i\) and \(\vec r_j\) is (for \(z_i > z_j\)):

% \begin{equation}
%   \label{optical-depth}
%   \tau_{ij} = \int_{z_i}^{z_j} \kappa(z,\nu_i)\frac{dz}{\mu_i},
% \end{equation}

% where \(\mu_i = \frac{(\vec{r}_j-\vec{r}_i)\cdot\hat{z}}{|\vec{r}_j-\vec{r}_i|}\) is the cosine of the zenith angle of the path through the medium.  

% In order to cover the entire volume of the atmosphere, it is also necessary that the expression for \(\tau_{ij}\) be averaged over the entire bin from \(z_i\) to \(z_{i+1}\) and \(z_j\) to \(z_{j+1}\). % NEED TO DEFINE CELLS RIGOROUSLY BEFORE DOING THIS PART.
% % define \delta_i as the thickness of cell i.




\subsection{Markov Chain Formalism}

% Mention somewhere that all quantities are averaged over all indexed variables

Now we may construct the single scattering matrix, \(Q\). We integrate the Equation~(\ref{equation:simplified-step}) over the frequency and angle variables to obtain the probability for a photon emitted at optical depth \(\tau_i\) to be scattered at \(\tau_j\),

\begin{equation}
  \label{single-scattering}
  Q_{ij} =  \sum_{a,b} \frac{w_a}{2} \frac{w_b}{2}
  \int_{-\infty}^\infty d\nu \int_{-\infty}^\infty d\nu'
  P_{ij}(\mu_a,\mu_b,\nu,\nu')
  % Need to define capital Phi_i as phi(\nu_i) integrated over the i^th freq bin.
\end{equation}

The multiply scattered profile consists of photons scattered zero, one, two, three times, and so on in a geometric progression \(G = 1 + Q + Q^2 + \cdots = (1 - Q)^{-1}\). %~\cite{geometric-series-of-matrices}

%It is shown by~\citet{esposito1978} that the observed intensity profile is given by $P_f = P_0 X$, where $P_0$ is the distribution of source photons and  $X$ is the solution to the linear system $(I - Q) X = R$. 
The final multiply-scattered profile, \(P_f\), is simply the product of the initial source distribution and the multiple-scattering matrix,

\begin{equation}
  P_f = P_0 (1-Q)^{-1}.
\end{equation}

The observed intensity profile can be obtained from the multiply scattered profile via another matrix, \(V\), which integrates the profile along sight lines. The intensity of the limb predicted by the model can be written,

\begin{equation}
  \hat y = V P_f.
\end{equation}

This can be rearranged to reveal that \(\hat y\) is the solution to a linear system of equations which is easier to compute than a matrix inversion:%~\citep{linear-system-is-easier-than-inversion}.

\begin{equation}
  \hat y(1-Q) = V P_0.
\end{equation}

This can be quite fast to calculate as long as the spatial bins are small enough that the errors incurred by averaging over the bin are not significant.

The plane-parallel assumption greatly simplifies the Feautrier solution.
It is possible to extend this solution to non-plane-parallel atmospheres, but at a high computational cost~\citep{anderson1977}.
% CHECK THAT THIS IS THE RIGHT REFERENCE!!
In multiple dimensions, optical depth no longer increases monotonically with physical location, and therefore cannot be substituted for the location variables directly.

The Markov chain method described in this paper can accommodate multidimensional atmospheres. 
The formula for the matrix elements does not assume a particular spatial relationship between them.
The single scattering matrix can be calculated on any finite set of grid points.
The main change to the formula would be that the optical depth must be computed between cells as \(\tau_{ij}\) and not simply the difference of tabulated depths from the top of the atmosphere, \(\tau_j - \tau_i\). 
%Averaging over 2D and 3D cells is much more difficult and computationally expensive to do correctly.
%An azimuthal scattering angle should also be included for two or three dimensional media.




% A graphical representation of the single- and multiple-scattering matrices is shown in Figure~\ref{graphical-matrices}.


%The scattering medium may be broken up into a finite number of cells.
%A single photon originating in cell $j$ may be scattered by an atom in cell $j$ with some probability, which we label $m_{ij}$. 
%If the source volume emission profile is $\s$, then the volume emission profile of photons that have scattered exactly once, $\J_1$ is related to the source through the transition matrix $\M$.
%%If photons are being produced by some source at a rate $S_i$, 
%%Let $\delta t$ be the characteristic time for photon scattering. %like mean free path/c
%%Let us assume that a photon can scatter at most one time in this interval.
%%If photons are produced in state $i$ at rate $S_i$, then number of photons in state $j$ after a single interval is given by
%
%\begin{equation}\label{first-scattering}
%  \J_1=\M\s
%  %J_j = m_{ij}S_i
%\end{equation}
%
%%Advancing the clock by $\delta t$, the singly-scattered photons are joined by another round of photons from the source, and these two populations combined look just like an emission source.%may scatter a second time, with probabilities given by the a transition matrix once again. 
%If the density of photons is small compared to the density of scatterers (if the medium is not very thick), the effects of the scattering on the population of scatterers is negligible, so the transition matrix is independent of scattered flux.
%Thus, the profile of photons scattered twice is,
%
%\begin{equation}\label{second-scattering}
%  \J_2 = \M^2\s
%\end{equation}
%
%The total volume emission rate includes photons that have been scattered zero times, once, twice, thrice, and so on. Thus, 
%
%%\begin{eqnarray}\label{geometric-sum}
    %%     \J = \left(I + \M + \M^2 + \cdots\right)\s,
%%\end{eqnarray}
%%
%%%GCT tells us that an absorbing Markov matrix is invertible... right?
%%where $I$ is the identity matrix.
%%
%%If the eigenvalues of the matrix $\M$ are all less than one, the geometric sum in \eqref{geometric-sum} will converge.
%%Under this condition, $(I-\M)$ is invertible and \eqref{geometric-sum} may be rewritten as 
%%
%%\begin{equation}\label{the-point}
%%  \J =(I-\M)^{-1}\s.
%%\end{equation}
%%
%%Since the matrix elements of $\M$ are probabilities, they are automatically non-negative. 
%%The sum of a row of $\M$ represents the probability that a photon born in cell $j$ will scatter somewhere in the medium. 
%%The photon may also be absorbed or simply escape the medium, so $\Sigma_i M_{ij}<1$.
%%Therefore, both conditions necessary for \eqref{the-point} are automatically satisfied for any absorbing Markov process.  
%
%


%Following~\ref{esposito1978}, the Markov chain approach allows us to calculate the steady-state behavior of the scattered emission from a matrix of probabilities for a single scattering event.
%One way to make sense of this is to notice that the light observed at any point in the scattering medium is composed of unscattered source photons, photons that have Scattered once before arriving at the detector, photons that have scattered twice, and so on.
%This means that if $\Ms$ maps the source profile to a profile of photons scattered exactly once, then the multiple scattering matrix, $M$, is a geometric series in $\Ms$:
%
%\begin{equation}
%M = 1 + \Ms + \Ms^2 + \Ms^3 + \cdots
%\end{equation}
%
%If the series converges, then the expression for $\M$ is simply, $\M=(1-\Ms)^{-1}$.







%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
